<!DOCTYPE html>
<html>
   <!-- Mirrored from light.pinsupreme.com/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 29 Mar 2018 05:48:52 GMT -->
   <head>
      <title>Admin Dashboard HTML Template</title>
      <meta charset="utf-8">
      <meta content="ie=edge" http-equiv="x-ua-compatible">
      <meta content="template language" name="keywords">
      <meta content="Tamerlan Soziev" name="author">
      <meta content="Admin dashboard html template" name="description">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <link href="favicon.png" rel="shortcut icon">
      <link href="apple-touch-icon.png" rel="apple-touch-icon">
      <link href="http://fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet" type="text/css">
      <link href="bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
      <link href="bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
      <link href="bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
      <link href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
      <link href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
      <link href="bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
      <link href="css/main4a76.css?version=4.3.0" rel="stylesheet">
   </head>
   <body class="auth-wrapper">
      <div class="all-wrapper menu-side with-pattern">
         <div class="auth-box-w">
            <div class="logo-w"><a href="index.php"><img alt="" src="img/logo-big.png"></a></div>
            <h4 class="auth-header">Login Form</h4>
            <form action="login.php" method="post">
               <div class="form-group">
                  <label for="">Username</label>
                  <input class="form-control" placeholder="Enter your username" name="username" type="text">
                  <div class="pre-icon os-icon os-icon-user-male-circle"></div>
               </div>
               <div class="form-group">
                  <label for="">Password</label>
                  <input class="form-control" placeholder="Enter your password" name="password" type="password">
                  <div class="pre-icon os-icon os-icon-fingerprint"></div>
               </div>
               <div class="buttons-w">
                  <button class="btn btn-primary">Log me in</button>
                  <div class="form-check-inline"><label class="form-check-label"><input class="form-check-input" type="checkbox">Remember Me</label></div>
               </div>
            </form>
         </div>
      </div>
   </body>
   <!-- Mirrored from light.pinsupreme.com/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 29 Mar 2018 05:48:52 GMT -->
</html>