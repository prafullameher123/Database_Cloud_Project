<?php
session_start();
$id = $_SESSION['id'];
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$db = 'r_and_d';
$conn = new mysqli($dbhost, $dbuser, $dbpass, $db);
$check_server_conn = "";
use phpseclib\Net\SSH2;
if(isset($_POST['ipsub']))
{
    if($_POST['login_type'] == "1")
    {
        require '../vendor/autoload.php';
        
            $ssh_ip = $_POST['ip'];
            $machine_name = $_POST['machine_name'];
            $ssh_username = $_POST['username'];
            $auth_type = "1";
            $ssh_password = $_POST['password'];
            $pub_key = htmlentities($_POST['pub_key']);
            $ssh = new SSH2($ssh_ip);
            if (!$ssh->login($ssh_username, $ssh_password)) {
                $check_server_conn = -1;
                //header("location: dashboard.php");
            }
            else
            {
                $machine_type = $ssh->exec('uname');
                $md5_password = md5($ssh_password);
                $out = $ssh->exec('echo "'.$pub_key.'" >> ~/.ssh/authorized_keys');
                $key_login = "insert into machine_info(machine_name, user_id, ip_address, machine_type, user_name, auth_type, public_key, password, created_at) values ('$machine_name', '$id', '$ssh_ip', '$machine_type', '$ssh_username', '$auth_type', '$pub_key', '$md5_password', now()) ";
                if ($conn->query($key_login) === TRUE) {
                    $check_server_conn = 1;
                    //print_r($check_server_conn); die();
                    //echo "<script>alert('Added Successfully');</script>";
                }
                else
                {
                    $check_server_conn = -1;
                    //echo "<script>alert('Error');</script>";

                }
                //header("location: dashboard.php");
            }
            /*echo "<pre>";
            print_r($out);*/

            die();
    }
    if($_POST['login_type'] == "2")
    {
        require '../vendor/autoload.php';
        $ssh_ip = $_POST['ip'];
        $ssh_username = $_POST['username'];
        $auth_type = "2";
        $ssh_password = $_POST['password'];
        $pub_key = "";
        $machine_name = $_POST['machine_name'];
        $ssh = new SSH2($ssh_ip);
        if (!$ssh->login($ssh_username, $ssh_password)) {
            $check_server_conn = -1;
            //header("location: dashboard.php");
        }
        else
        {
            $machine_type = $ssh->exec('uname');
            $md5_password = md5($ssh_password);
            $pass_login = "insert into machine_info(machine_name, user_id, ip_address, machine_type, user_name, auth_type, public_key, password, created_at) values ('$machine_name','$id', '$ssh_ip', '$machine_type', '$ssh_username', '$auth_type', '$pub_key', '$md5_password', now()) ";
            if ($conn->query($pass_login) === TRUE) {
                $check_server_conn = 1;
                //print_r($check_server_conn); die();
                //echo "<script>alert('Added Successfully');</script>";
            }
            else
            {
                $check_server_conn = -1;
                //echo "<script>alert('Error');</script>";

            }
            //header("location: dashboard.php");
        }
        /*echo $ssh->exec('uname');
        echo "<br>";
        echo $ssh->exec('uname -a');
        echo "<br>";
        echo $ssh->exec('pwd');
        $output = $ssh->exec('ls -la');
        echo "<pre>";
        print_r($output);
        die();*/
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Ajatus Software</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
</head>
<style type="text/css">
    body.modal-open {
        overflow: hidden;
    }
</style>
<body>
    <div class="wrapper">
        <div class="sidebar" data-image="../assets/img/sidebar-5.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="dashboard.php" class="simple-text">
                        Ajatus Software
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">
                            <i class="nc-icon nc-chart-pie-35"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand" href="dashboard.php"> Dashboard </a>
                    
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <!-- <ul class="nav navbar-nav mr-auto">
                            <li class="dropdown nav-item">
                                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                    <i class="nc-icon nc-planet"></i>
                                    <span class="notification">5</span>
                                    <span class="d-lg-none">Notification</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Notification 1</a>
                                    <a class="dropdown-item" href="#">Notification 2</a>
                                    <a class="dropdown-item" href="#">Notification 3</a>
                                    <a class="dropdown-item" href="#">Notification 4</a>
                                    <a class="dropdown-item" href="#">Another notification</a>
                                </ul>
                            </li>
                        </ul> -->
                        <ul class="navbar-nav ml-auto">
                            
                            <li class="nav-item">
                                <a class="nav-link" href="signout.php">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">

                        <?php if($check_server_conn == 1){ ?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> Conection to your Server Created successfully.
                        </div>
                       <?php } ?>
                       <?php if($check_server_conn == -1){ ?>
                        <div class="alert alert-danger">
                            <strong>Oops!</strong> Some Error in Connection.
                        </div>
                       <?php } ?>
                        <div class="col-md-8">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="cursor: pointer;" id="add_modal_btn" onclick="add_modal_btn()">Add New</button>
                            <div class="modal fade" id="myModal" role="dialog"  style="margin-top: -106px">
                                <div class="modal-dialog">
                                
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post">
                                            <div class="first_div" style="display: block">
                                                <div class="form-group">
                                                    <label for="email">Machine Name:</label>
                                                    <input type="text" name="machine_name" class="form-control" id="machine_name" onchange="validate()">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">IP Address:</label>
                                                    <input type="text" name="ip" class="form-control" id="ip" onchange="validate()">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">User Name:</label>
                                                    <input type="text" name="username" class="form-control" id="u_name" onchange="validate()">
                                                </div>
                                                <div class="form-group">
                                                    <label for="authentication">Authentication Type</label>
                                                    <select class="form-control" id="login_type" name="login_type" onchange="myFunction(); validate()">
                                                        <option>--select--</option>
                                                        <option value="1">Key Based Login</option>
                                                        <option value="2">Password Based Login</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" id="pub_key" style="display: none">
                                                    <label for="pub_key">Key:</label>
                                                    <input type="text" name="pub_key" class="form-control" id="pub_key" onchange="validate()">
                                                </div>
                                                <div class="form-group" id="password_div" style="display: none">
                                                    <label for="pwd">Password:</label>
                                                    <input type="password" name="password" class="form-control" id="pwd" onchange="validate()">
                                                </div>
                                                <button type="button" id="next" class="btn btn-default" style="float: right;" disabled="true" onclick="show_next_div()">next</button>
                                            </div>
                                            <div class="second_div" style="display: none">
                                                <div class="form-group">
                                                    <label for="db_type">Select Database Type:</label><br>
                                                    <input type="radio" name = "db_type" value="ms_sql" style="margin-left: 20px;">
                                                    <img src = "./images/mssql.png" width="100px">
                                                    <input type="radio" name = "db_type" value="mysql" style="margin-left: 20px;">
                                                    <img src = "./images/mysql.png" width="100px">
                                                    <input type="radio" name = "db_type" value="mongodb" style="margin-left: 20px;">
                                                    <img src = "./images/mongodb.png" width="100px">
                                                </div>
                                                <div class="form-group">
                                                    <label for="db_uname">Database Username:</label>
                                                    <input type="text" name="db_uname" class="form-control" id = "db_uname">
                                                </div>
                                                <div class="form-group">
                                                    <label for="db_pass">Database Password:</label>
                                                    <input type="text" name="db_pass" class="form-control" id = "db_pass">
                                                </div>
                                                <button type="submit" name ="ipsub" class="btn btn-default" style="float: right;">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                      
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th>Machine Type</th>
                                    <th>Machine IP</th>
                                    <th>Username</th>
                                    <th>Operation</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    
                                <?php
                                      $machine_details = "Select * from machine_info where user_id = '$id'";
                                      $machine_result = $conn->query($machine_details);
                                      if($machine_result->num_rows>0)
                                      {
                                         while($rows = $machine_result->fetch_assoc())
                                         {
                                            $m_id = $rows['id'];
                                            $type = $rows['machine_type'];
                                            $ip = $rows['ip_address'];
                                            $uname = $rows['user_name'];
                                            echo "<tr><td>$type</td><td>$ip</td><td>$uname</td><td>
                                                    <form action = 'view_detail.php?id=".$m_id."' method ='post'>
                                                        <button class='btn btn-info btn-md' style= 'cursor: pointer;'>View</button>
                                                    </form>
                                                  </td></tr>";
                                         }
                                      }

                                ?>
                                  
                                </tbody>
                              </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav>
                        <ul class="footer-menu">
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            <a href="dashboard.php">Ajatus Software</a>, made with love for a better web
                        </p>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
    <!--   -->
    <!-- <div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-cog fa-2x"> </i>
        </a>

        <ul class="dropdown-menu">
			<li class="header-title"> Sidebar Style</li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger">
                    <p>Background Image</p>
                    <label class="switch">
                        <input type="checkbox" data-toggle="switch" checked="" data-on-color="primary" data-off-color="primary"><span class="toggle"></span>
                    </label>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger background-color">
                    <p>Filters</p>
                    <div class="pull-right">
                        <span class="badge filter badge-black" data-color="black"></span>
                        <span class="badge filter badge-azure" data-color="azure"></span>
                        <span class="badge filter badge-green" data-color="green"></span>
                        <span class="badge filter badge-orange" data-color="orange"></span>
                        <span class="badge filter badge-red" data-color="red"></span>
                        <span class="badge filter badge-purple active" data-color="purple"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li class="header-title">Sidebar Images</li>

            <li class="active">
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="../assets/img/sidebar-1.jpg" alt="" />
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="../assets/img/sidebar-3.jpg" alt="" />
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="..//assets/img/sidebar-4.jpg" alt="" />
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="../assets/img/sidebar-5.jpg" alt="" />
                </a>
            </li>

            <li class="button-container">
                <div class="">
                    <a href="http://www.creative-tim.com/product/light-bootstrap-dashboard" target="_blank" class="btn btn-info btn-block btn-fill">Download, it's free!</a>
                </div>
            </li>

            <li class="header-title pro-title text-center">Want more components?</li>

            <li class="button-container">
                <div class="">
                    <a href="http://www.creative-tim.com/product/light-bootstrap-dashboard-pro" target="_blank" class="btn btn-warning btn-block btn-fill">Get The PRO Version!</a>
                </div>
            </li>

            <li class="header-title" id="sharrreTitle">Thank you for sharing!</li>

            <li class="button-container">
				<button id="twitter" class="btn btn-social btn-outline btn-twitter btn-round sharrre"><i class="fa fa-twitter"></i> · 256</button>
                <button id="facebook" class="btn btn-social btn-outline btn-facebook btn-round sharrre"><i class="fa fa-facebook-square"></i> · 426</button>
            </li>
        </ul>
    </div>
</div>
 -->
</body>
<script type="text/javascript">
    function myFunction(){
        if($("#login_type").val() == 1){
            //$("#pub_key").css("display", "block");
            document.getElementById("pub_key").style.display = "block";
            document.getElementById("password_div").style.display = "block";
        }
        else if($("#login_type").val() == 2){
            document.getElementById("password_div").style.display = "block";
            document.getElementById("pub_key").style.display = "none";
        }else{
            document.getElementById("pub_key").style.display = "none";
            document.getElementById("password_div").style.display = "none";
        }
    }
    function validate(){
        if($("#ip").val() != "" && $("#ip").val() != null){
            if($("#u_name").val() != "" && $("#u_name").val() != null){
                if($("#login_type").val() == '1'){
                    /*if($("#pub_key").val() != "" && $("#pub_key").val() != null){
                        if($("#pwd").val() != "" && $("#pwd").val() != null){*/
                            document.getElementById("next").disabled = false;
                        /*}
                    }*/
                }
                else if($("#login_type").val() == '2'){
                    if($("#pwd").val() != "" && $("#pwd").val() != null){
                        document.getElementById("next").disabled = false;
                    }
                }else{

                }
            }
        }
    }
    function add_modal_btn(){
        $(".first_div").css("display", "block");
        $(".second_div").css("display", "none");
    }
    function show_next_div(){
        $(".first_div").css("display", "none");
        $(".second_div").css("display", "block");
    }
    </script>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Chartist Plugin  -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="../assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>

</html>