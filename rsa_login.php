<?php
require __DIR__ . '/vendor/autoload.php';

$loader = new \Composer\Autoload\ClassLoader();
$loader->addPsr4('phpseclib\\', __DIR__ . 'vendor\phpseclib\phpseclib');
$loader->register();

use phpseclib\Crypt\RSA;
use phpseclib\Net\SSH2;

$key = new RSA();
$key->loadKey(file_get_contents('id_rsa.txt'));

// Domain can be an IP too
$ssh = new SSH2('efleet-dev.ajatus.in');
if (!$ssh->login('efleetsystems', $key)) {
    exit('Login Failed');
}

echo $ssh->exec('uname');
echo "<br>";
echo $ssh->exec('uname -a');
echo "<br>";
echo $ssh->exec('pwd');
$output = $ssh->exec('ls -la');
echo "<pre>";
print_r($output);
die();
?>